const config = require('../../configs/config');
const ArchetypeApplicationService = require('../../services/archetype');
const applicationService = new ArchetypeApplicationService(config);

jest.mock('../../__mocks__/request');

it('correct message is generated for async/await method1', async () => {
  const requiredParam = "valor requerido";
  const optionalParam = "valor opcional";
  const expectedValue = "LAMBDA: lambdaMethod1 called with requiredParam: "+requiredParam+" and optionalParam: "+optionalParam;
  expect(await applicationService.method1(requiredParam, optionalParam)).toBe(expectedValue);
});

it('correct message is generated for async/await method2', async () => {
  const otherParam = "other";
  expect(await applicationService.method2(otherParam)).toContain("lambdaMethod2");
});


it('correct message is generated for async/await method3', async () => {
  //expect.assertions(1);
  const data =  await applicationService.method3(1);
  //expect(data).contains('title'); // TODO: Mock request based on jest.mock('../../__mocks__/request')
});

