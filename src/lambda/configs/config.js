module.exports = {
  url1: process.env.LAMBDA_URL1,
  method1: process.env.LAMBDA_METHOD1,
  method2: process.env.LAMBDA_METHOD2,
  method3: process.env.LAMBDA_METHOD3,
  ssmEnvPlain: process.env.SSM_ENV_PLAIN,
  ssmEnvSecret: process.env.SSM_ENV_SECRET,
  ssmEnvNotDefined: process.env.SSM_ENV_NOT_DEFINED
};