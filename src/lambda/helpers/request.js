const AWSXRay = require('aws-xray-sdk');
//const https   = AWSXRay.captureHTTPs(require('https'));
const https   = require('https');

module.exports.pathSeparator = "/";
module.exports.get = function(url) {
    return new Promise((resolve, reject) => {
        https.get(url, (response) => {
            let data = '';
            response.on('data', (chunk) => {data += chunk;});
            response.on('end', () => {resolve(JSON.parse(data));});
        }).on('error', (err) => {
            console.log(err);
            reject(err);
        });
    })
};