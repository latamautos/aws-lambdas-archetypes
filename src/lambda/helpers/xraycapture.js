const AWSXRay = require('aws-xray-sdk');

AWSXRay.capturePromise(); // promise prototype
AWSXRay.capturePromise = function () {}

module.exports.getSegment = function () {
    return AWSXRay.getSegment();
}

module.exports.captureAllOutgoingAWSRequests = function () {
    return AWSXRay.captureAWS(require('aws-sdk'));
}

module.exports.captureAsyncFunc = function (fname, fmessage, f) {
    return new Promise(function (resolve, reject) {
        AWSXRay.captureAsyncFunc(fname, segment => {
            f(segment).then(
                (result) => {
                    segment.addAnnotation('message', fmessage);
                    segment.addMetadata('message', fmessage);
                    segment.close();
                    resolve(result);
                },
                (error) => {
                    segment.addAnnotation('error', error);
                    segment.addMetadata('error', error);
                    segment.close(error);
                    reject(error);
                }
            )
        })
    })
}

module.exports.captureSyncFunc = function (fname, f) {
    AWSXRay.captureFunc(fname, f);
}