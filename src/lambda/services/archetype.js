const request = require('../helpers/request');

class ArchetypeApplicationService {

  constructor(config) {
    this.config = config;
  }

  async method1(requiredParam, optionalParam) {
    const result = 'LAMBDA: lambdaMethod1 called with requiredParam: ' + requiredParam + ' and optionalParam: ' + optionalParam;
    return await result;
  }

  async method2(otherParam) {
    const result =  'LAMBDA: lambdaMethod2 called with otherParam: ' + otherParam + ' / SSM ENVS '
        + '(SSM_ENV_PLAIN:' + this.config.ssmEnvPlain + ', SSM_ENV_SECRET:' + this.config.ssmEnvSecret + ', SSM_ENV_NOT_DEFINED:' + this.config.ssmEnvNotDefined + ')';
    return await result;
  }

  async method3(urlParam) {
    return await request.get(this.config.url1+request.pathSeparator+urlParam);
  }

}

module.exports = ArchetypeApplicationService;