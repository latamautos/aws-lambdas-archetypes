const motherData = {
    1: {"userId": 1, "id": 1, "title": "title1", "body": "body1"},
    2: {"userId": 2, "id": 2, "title": "title2", "body": "body2"}
};

module.exports.pathSeparator = "/";
module.exports.get = function (url) {
    return new Promise((resolve, reject) => {
        const id = parseInt(url.substr('/posts/'.length), 10);
        process.nextTick(() =>
            motherData[id]
                ? resolve(motherData[id])
                : reject({
                    error: 'MotherData with ' + id + ' not found.',
                }),
        );
    });
}