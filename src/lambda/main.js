'use strict';

const config = require('./configs/config');
const logger = require('./helpers/logger');
const tracer = require('./helpers/xraycapture');
const ArchetypeApplicationService = require('./services/archetype');

exports.graphql = async (event) => {

    const applicationService = new ArchetypeApplicationService(config);
    const segment = tracer.getSegment;
    tracer.captureAllOutgoingAWSRequests();

    function mySyncFunction() {
        logger.log("syncFunction", "Any sync main function");
        return "syncFunction";
    }

    function eventRouting(method) {
        const events = {
            [config.method1]: function () {
                const requiredParam = event.arguments.requiredParam;
                const optionalParam = event.arguments.optionalParam;
                return applicationService.method1(requiredParam, optionalParam);
            },
            [config.method2]: function () {
                const otherParam = event.arguments.otherParam;
                return applicationService.method2(otherParam);
            },
            [config.method3]: function () {
                const urlParam = event.arguments.urlParam;
                tracer.captureSyncFunc("syncFunction called from method3",(segment) => mySyncFunction());
                return tracer.captureAsyncFunc('asyncFunction called from method3', 'any message for annotation and metadata' + urlParam,  (segment) => applicationService.method3(urlParam));
            },
            'notImplemented': function () {
                const error = "LAMBDA METHOD NOT IMPLEMENTED";
                throw new Error(error);
            }
        };
        return (events[method] || events['notImplemented'])();
    }

    try {
        return eventRouting(event.field);
    } catch (err) {
        logger.log(event.field, err);
        return err;
    }

};